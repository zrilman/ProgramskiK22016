#include "Kolokvijum.h"
#include <fstream>


namespace Kolokvijum
{
	class Person : public Printable
	{
	public:
		Person(std::string, const int = 0);
		bool operator== (const Person&);
		int getJmbg() const;
		std::string getName() const;
	protected:
		void print(std::ostream&) override;
	private:
		void run1() final {}
		void run2() final {}
	};
}


namespace std
{
	template<>
	struct hash<Kolokvijum::Person>
	{
		size_t operator()(const Kolokvijum::Person& x) const
		{
			return hash<int>()(x.getJmbg());
		}
	};
}