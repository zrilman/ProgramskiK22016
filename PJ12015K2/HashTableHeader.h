#pragma once
#include <vector>
#include <fstream>
namespace Kolokvijum
{
	template<class T, typename hash = std::hash<Person>>
	class HashTable
	{
	public:
		HashTable(unsigned) noexcept;
		HashTable(const HashTable&);
		HashTable(HashTable&&) noexcept;
		~HashTable();
		HashTable& operator= (const HashTable&);
		HashTable& operator= (HashTable&&) noexcept;
		void insert(T&);
		bool exists(const T&) const noexcept;
		void print(std::ostream&);
	protected:
	private:
		T** arr;
		unsigned cap, numOfElements;
		std::vector<T> vec;

		void copy(const HashTable&);
		void move(HashTable&&) noexcept;
		inline bool isFull() const noexcept;
	};



	template<class T, typename hash>
	HashTable<T, hash>::HashTable(unsigned ccap) noexcept: cap(ccap)
	{
		arr = new T*[cap];
		std::fill(arr, arr + cap, nullptr);
	}

	template<class T, typename hash>
	HashTable<T, hash>::HashTable(const HashTable& other) { copy(other); }

	template<class T, typename hash>
	HashTable<T, hash>::HashTable(HashTable&& other) noexcept { move(std::move(other)); }

	template<class T, typename hash>
	HashTable<T, hash>::~HashTable()
	{
		for (int i = 0; i < numOfElements; ++i)
			delete arr[i];
		delete[] arr;
		numOfElements = cap = 0;
	}

	template<class T, typename hash>
	HashTable<T, hash>& HashTable<T, hash>::operator= (const HashTable& other)
	{
		if (this != &other)
			copy(other);
		return *this;
	}

	template<class T, typename hash>
	HashTable<T, hash>& HashTable<T, hash>::operator= (HashTable&& other) noexcept
	{
		if (this != &other)
			move(std::move(other));
		return *this;
	}

	template<class T, typename hash>
	void HashTable<T, hash>::insert(T& info)
	{
		if (isFull())
			throw std::exception("Hash table full.\n");
		int index = hash()(info) % cap;
		if (exists(info))
			vec.push_back(info);
		else
		{
			numOfElements++;
			arr[index] = new T(info);
		}
	}

	template<class T, typename hash>
	bool HashTable<T, hash>::exists(const T& info) const noexcept
	{
		size_t index = hash()(info) % cap;
		return arr[index] != nullptr;
	}

	template<class T, typename hash>
	void HashTable<T, hash>::copy(const HashTable& other)
	{
		cap = other.cap;
		numOfElements = other.numOfElements;
		arr = new T*[cap];
		for (int i = 0; i < cap; i++)
			arr[i] = new T(*other.arr[i]);
	}

	template<class T, typename hash>
	void HashTable<T, hash>::move(HashTable&& other) noexcept
	{
		cap = other.cap;
		numOfElements = other.numOfElements;
		arr = other.arr;
		other.arr = nullptr;
	}

	template<class T, typename hash>
	inline bool HashTable<T, hash>::isFull() const noexcept
	{
		return cap == numOfElements;
	}

	template<class T, typename hash>
	void HashTable<T, hash>::print(std::ostream& os)
	{
		os << "Hash Table:" << std::endl;
		for (int i = 0; i < cap; ++i)
			if (arr[i] != nullptr)
				os << *arr[i];
		os << "Vector: " << std::endl;
		for (auto& it : vec)
			os << it;
	}

}