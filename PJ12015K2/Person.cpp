#include "Person.h"
#include <iomanip>
#include <string>

namespace Kolokvijum
{
	Person::Person(std::string nname, const int iid)
	{
		name = nname;
		id = iid;
	}

	bool Person::operator== (const Person& other)
	{
		return id == other.id;
	}

	void Person::print(std::ostream& os)
	{
		os << std::setw(14) << std::left << id << ' ' << name << std::endl;
	}

	int Person::getJmbg() const
	{
		return id;
	}

	std::string Person::getName() const
	{
		return name;
	}
}
