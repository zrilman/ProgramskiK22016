#pragma once 
#include <iostream> 
#include <string> 
#include <vector>
#include <algorithm>


namespace Kolokvijum
{
	class Printable
	{
		friend std::ostream& operator<< (std::ostream& str, Printable& pr)
		{
			pr.print(str);
			return str;

		}
	public:
		Printable() = default;
		Printable(const char* name) : name(name)
		{
			id = ++count;
		}
		virtual ~Printable() {}
	protected:
		std::string name; int id;
		virtual void print(std::ostream& str)
		{
			//run1();
			printName(str);
			//run2();
		}
		virtual void printName(std::ostream& str) { str << name; }
		virtual void run1()
		{
			std::cout << "s" << id++;
		}
		virtual void run2()
		{
			std::cout << "e" << id;
		}
	private:
		static int count;
	};
}