#include "Person.h"
#include "HashTableHeader.h"
using namespace Kolokvijum;

void main()
{
	Person p1("Dragan", 4);
	Person p2("Igor", 3);
	Person p3("Dejan", 1);
	Person p4("Mladen", 2);
	Person p5("Mladen", 13121996);
	Printable p[3] = { p1,p2,p3 };
	std::cout << p1 << p2 << p3 << p4 << std::endl;
	for (int i = 0; i < 3; ++i)
		std::cout << p[i] << ' ';
	std::cout << std::endl;
	HashTable <Person> ht(4);
	try {
		ht.insert(p1);
		ht.insert(p2);
		ht.insert(p3);
		ht.insert(p4);
		ht.insert(p5);
	}
	catch (std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}
	ht.print(std::cout);

	std::ofstream ofs("out.txt");
	if (ofs)
	{
		ht.print(ofs);
	}
	ofs.close();

	std::cin.ignore();
	std::cin.get();
}